﻿namespace QuanLyKho
{
    partial class fAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tbNewPass = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbOldPass = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tbChangePass = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbDeleteAccount = new System.Windows.Forms.ComboBox();
            this.btnXOA = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chbEditRole = new System.Windows.Forms.CheckBox();
            this.tbEditUsername = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.tbEditDisplayName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkAD = new System.Windows.Forms.CheckBox();
            this.btnDangKy = new System.Windows.Forms.Button();
            this.MK = new System.Windows.Forms.TextBox();
            this.TK = new System.Windows.Forms.TextBox();
            this.HoVaTen = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbDeleteSP = new System.Windows.Forms.ComboBox();
            this.btnXoaSP = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cbNewMASP = new System.Windows.Forms.ComboBox();
            this.nmNewGia = new System.Windows.Forms.NumericUpDown();
            this.cbNewNCC = new System.Windows.Forms.ComboBox();
            this.EditSP = new System.Windows.Forms.Button();
            this.tbNewname = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cbIdSuplier = new System.Windows.Forms.ComboBox();
            this.nmAddPrice = new System.Windows.Forms.NumericUpDown();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tbTenSp = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNewGia)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmAddPrice)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1290, 693);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1282, 664);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tài Khoản";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.groupBox7);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Location = new System.Drawing.Point(3, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1273, 652);
            this.panel1.TabIndex = 1;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tbNewPass);
            this.groupBox7.Controls.Add(this.label7);
            this.groupBox7.Controls.Add(this.tbOldPass);
            this.groupBox7.Controls.Add(this.label5);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.tbChangePass);
            this.groupBox7.Controls.Add(this.label4);
            this.groupBox7.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(620, 322);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(611, 285);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Đổi Mật Khẩu";
            // 
            // tbNewPass
            // 
            this.tbNewPass.Location = new System.Drawing.Point(235, 175);
            this.tbNewPass.Name = "tbNewPass";
            this.tbNewPass.Size = new System.Drawing.Size(309, 28);
            this.tbNewPass.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(57, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 21);
            this.label7.TabIndex = 9;
            this.label7.Text = "Mật Khẩu Mới :";
            // 
            // tbOldPass
            // 
            this.tbOldPass.Location = new System.Drawing.Point(235, 123);
            this.tbOldPass.Name = "tbOldPass";
            this.tbOldPass.Size = new System.Drawing.Size(309, 28);
            this.tbOldPass.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 21);
            this.label5.TabIndex = 7;
            this.label5.Text = "Mật Khẩu Cũ :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(449, 222);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 36);
            this.button1.TabIndex = 6;
            this.button1.Text = "Sửa";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tbChangePass
            // 
            this.tbChangePass.Location = new System.Drawing.Point(235, 70);
            this.tbChangePass.Name = "tbChangePass";
            this.tbChangePass.Size = new System.Drawing.Size(309, 28);
            this.tbChangePass.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(57, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 21);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tài Khoản : ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbDeleteAccount);
            this.groupBox3.Controls.Add(this.btnXOA);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(3, 322);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(611, 285);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Xóa Tài Khoản";
            // 
            // cbDeleteAccount
            // 
            this.cbDeleteAccount.FormattingEnabled = true;
            this.cbDeleteAccount.Location = new System.Drawing.Point(235, 111);
            this.cbDeleteAccount.Name = "cbDeleteAccount";
            this.cbDeleteAccount.Size = new System.Drawing.Size(284, 28);
            this.cbDeleteAccount.TabIndex = 7;
            // 
            // btnXOA
            // 
            this.btnXOA.Location = new System.Drawing.Point(341, 158);
            this.btnXOA.Name = "btnXOA";
            this.btnXOA.Size = new System.Drawing.Size(95, 36);
            this.btnXOA.TabIndex = 6;
            this.btnXOA.Text = "Xóa";
            this.btnXOA.UseVisualStyleBackColor = true;
            this.btnXOA.Click += new System.EventHandler(this.btnXOA_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(60, 111);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 21);
            this.label9.TabIndex = 0;
            this.label9.Text = "Tên Tài Khoản : ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chbEditRole);
            this.groupBox2.Controls.Add(this.tbEditUsername);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btnEdit);
            this.groupBox2.Controls.Add(this.tbEditDisplayName);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(620, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(611, 295);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thay Đổi Thông Tin Tài Khoản";
            // 
            // chbEditRole
            // 
            this.chbEditRole.AutoSize = true;
            this.chbEditRole.Location = new System.Drawing.Point(235, 182);
            this.chbEditRole.Name = "chbEditRole";
            this.chbEditRole.Size = new System.Drawing.Size(82, 25);
            this.chbEditRole.TabIndex = 8;
            this.chbEditRole.Text = "Admin";
            this.chbEditRole.UseVisualStyleBackColor = true;
            // 
            // tbEditUsername
            // 
            this.tbEditUsername.Location = new System.Drawing.Point(235, 108);
            this.tbEditUsername.Name = "tbEditUsername";
            this.tbEditUsername.Size = new System.Drawing.Size(309, 28);
            this.tbEditUsername.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(59, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 21);
            this.label8.TabIndex = 7;
            this.label8.Text = "Tài Khoản :";
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(449, 249);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(95, 36);
            this.btnEdit.TabIndex = 6;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // tbEditDisplayName
            // 
            this.tbEditDisplayName.Location = new System.Drawing.Point(235, 50);
            this.tbEditDisplayName.Name = "tbEditDisplayName";
            this.tbEditDisplayName.Size = new System.Drawing.Size(309, 28);
            this.tbEditDisplayName.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(117, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Hiển Thị :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkAD);
            this.groupBox1.Controls.Add(this.btnDangKy);
            this.groupBox1.Controls.Add(this.MK);
            this.groupBox1.Controls.Add(this.TK);
            this.groupBox1.Controls.Add(this.HoVaTen);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 21);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(611, 295);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Đăng Ký";
            // 
            // checkAD
            // 
            this.checkAD.AutoSize = true;
            this.checkAD.Location = new System.Drawing.Point(235, 210);
            this.checkAD.Name = "checkAD";
            this.checkAD.Size = new System.Drawing.Size(82, 25);
            this.checkAD.TabIndex = 7;
            this.checkAD.Text = "Admin";
            this.checkAD.UseVisualStyleBackColor = true;
            // 
            // btnDangKy
            // 
            this.btnDangKy.Location = new System.Drawing.Point(449, 249);
            this.btnDangKy.Name = "btnDangKy";
            this.btnDangKy.Size = new System.Drawing.Size(95, 36);
            this.btnDangKy.TabIndex = 6;
            this.btnDangKy.Text = "Đăng Ký";
            this.btnDangKy.UseVisualStyleBackColor = true;
            this.btnDangKy.Click += new System.EventHandler(this.btnDangKy_Click_1);
            // 
            // MK
            // 
            this.MK.Location = new System.Drawing.Point(235, 164);
            this.MK.Name = "MK";
            this.MK.Size = new System.Drawing.Size(309, 28);
            this.MK.TabIndex = 5;
            // 
            // TK
            // 
            this.TK.Location = new System.Drawing.Point(235, 108);
            this.TK.Name = "TK";
            this.TK.Size = new System.Drawing.Size(309, 28);
            this.TK.TabIndex = 4;
            // 
            // HoVaTen
            // 
            this.HoVaTen.Location = new System.Drawing.Point(235, 54);
            this.HoVaTen.Name = "HoVaTen";
            this.HoVaTen.Size = new System.Drawing.Size(309, 28);
            this.HoVaTen.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 171);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Mật Khẩu : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên Tài Khoản :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên Hiển Thị :";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1282, 664);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sản Phẩm";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cbDeleteSP);
            this.groupBox4.Controls.Add(this.btnXoaSP);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(376, 340);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(611, 285);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Xóa";
            // 
            // cbDeleteSP
            // 
            this.cbDeleteSP.AccessibleRole = System.Windows.Forms.AccessibleRole.PageTabList;
            this.cbDeleteSP.FormattingEnabled = true;
            this.cbDeleteSP.Location = new System.Drawing.Point(193, 81);
            this.cbDeleteSP.Name = "cbDeleteSP";
            this.cbDeleteSP.Size = new System.Drawing.Size(229, 28);
            this.cbDeleteSP.TabIndex = 7;
            // 
            // btnXoaSP
            // 
            this.btnXoaSP.Location = new System.Drawing.Point(274, 158);
            this.btnXoaSP.Name = "btnXoaSP";
            this.btnXoaSP.Size = new System.Drawing.Size(95, 36);
            this.btnXoaSP.TabIndex = 6;
            this.btnXoaSP.Text = "Xóa";
            this.btnXoaSP.UseVisualStyleBackColor = true;
            this.btnXoaSP.Click += new System.EventHandler(this.btnXoaSP_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(78, 84);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 21);
            this.label12.TabIndex = 0;
            this.label12.Text = "MãSP";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.cbNewMASP);
            this.groupBox5.Controls.Add(this.nmNewGia);
            this.groupBox5.Controls.Add(this.cbNewNCC);
            this.groupBox5.Controls.Add(this.EditSP);
            this.groupBox5.Controls.Add(this.tbNewname);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(650, 39);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(611, 295);
            this.groupBox5.TabIndex = 11;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sửa";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(48, 226);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 21);
            this.label10.TabIndex = 12;
            this.label10.Text = "SP";
            // 
            // cbNewMASP
            // 
            this.cbNewMASP.AccessibleRole = System.Windows.Forms.AccessibleRole.PageTabList;
            this.cbNewMASP.FormattingEnabled = true;
            this.cbNewMASP.Location = new System.Drawing.Point(235, 219);
            this.cbNewMASP.Name = "cbNewMASP";
            this.cbNewMASP.Size = new System.Drawing.Size(229, 28);
            this.cbNewMASP.TabIndex = 11;
            // 
            // nmNewGia
            // 
            this.nmNewGia.Location = new System.Drawing.Point(235, 115);
            this.nmNewGia.Maximum = new decimal(new int[] {
            -559939584,
            902409669,
            54,
            0});
            this.nmNewGia.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nmNewGia.Name = "nmNewGia";
            this.nmNewGia.Size = new System.Drawing.Size(129, 28);
            this.nmNewGia.TabIndex = 10;
            this.nmNewGia.Value = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            // 
            // cbNewNCC
            // 
            this.cbNewNCC.FormattingEnabled = true;
            this.cbNewNCC.Location = new System.Drawing.Point(235, 168);
            this.cbNewNCC.Name = "cbNewNCC";
            this.cbNewNCC.Size = new System.Drawing.Size(153, 28);
            this.cbNewNCC.TabIndex = 9;
            // 
            // EditSP
            // 
            this.EditSP.Location = new System.Drawing.Point(493, 234);
            this.EditSP.Name = "EditSP";
            this.EditSP.Size = new System.Drawing.Size(95, 36);
            this.EditSP.TabIndex = 6;
            this.EditSP.Text = "Sửa";
            this.EditSP.UseVisualStyleBackColor = true;
            this.EditSP.Click += new System.EventHandler(this.EditSP_Click);
            // 
            // tbNewname
            // 
            this.tbNewname.Location = new System.Drawing.Point(235, 54);
            this.tbNewname.Name = "tbNewname";
            this.tbNewname.Size = new System.Drawing.Size(309, 28);
            this.tbNewname.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(48, 171);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 21);
            this.label13.TabIndex = 2;
            this.label13.Text = "NCC";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(48, 115);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(36, 21);
            this.label14.TabIndex = 1;
            this.label14.Text = "Giá";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(48, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 21);
            this.label15.TabIndex = 0;
            this.label15.Text = "Tên";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cbIdSuplier);
            this.groupBox6.Controls.Add(this.nmAddPrice);
            this.groupBox6.Controls.Add(this.btnAdd);
            this.groupBox6.Controls.Add(this.tbTenSp);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(33, 39);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(611, 295);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Thêm";
            // 
            // cbIdSuplier
            // 
            this.cbIdSuplier.FormattingEnabled = true;
            this.cbIdSuplier.Location = new System.Drawing.Point(235, 166);
            this.cbIdSuplier.Name = "cbIdSuplier";
            this.cbIdSuplier.Size = new System.Drawing.Size(153, 28);
            this.cbIdSuplier.TabIndex = 8;
            this.cbIdSuplier.SelectedIndexChanged += new System.EventHandler(this.cbIdSuplier_SelectedIndexChanged);
            // 
            // nmAddPrice
            // 
            this.nmAddPrice.Location = new System.Drawing.Point(235, 113);
            this.nmAddPrice.Maximum = new decimal(new int[] {
            -559939584,
            902409669,
            54,
            0});
            this.nmAddPrice.Minimum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nmAddPrice.Name = "nmAddPrice";
            this.nmAddPrice.Size = new System.Drawing.Size(120, 28);
            this.nmAddPrice.TabIndex = 7;
            this.nmAddPrice.Value = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(343, 214);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 36);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tbTenSp
            // 
            this.tbTenSp.Location = new System.Drawing.Point(235, 54);
            this.tbTenSp.Name = "tbTenSp";
            this.tbTenSp.Size = new System.Drawing.Size(309, 28);
            this.tbTenSp.TabIndex = 3;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(48, 171);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 21);
            this.label16.TabIndex = 2;
            this.label16.Text = "NCC";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(48, 115);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(36, 21);
            this.label17.TabIndex = 1;
            this.label17.Text = "Giá";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(48, 57);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 21);
            this.label18.TabIndex = 0;
            this.label18.Text = "Tên";
            // 
            // fAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1308, 717);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "fAdmin";
            this.Text = "Admin";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmNewGia)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmAddPrice)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnXOA;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkAD;
        private System.Windows.Forms.Button btnDangKy;
        private System.Windows.Forms.TextBox MK;
        private System.Windows.Forms.TextBox TK;
        private System.Windows.Forms.TextBox HoVaTen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnXoaSP;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button EditSP;
        private System.Windows.Forms.TextBox tbNewname;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox tbTenSp;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tbNewPass;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbOldPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbChangePass;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbDeleteAccount;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.TextBox tbEditDisplayName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chbEditRole;
        private System.Windows.Forms.TextBox tbEditUsername;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbIdSuplier;
        private System.Windows.Forms.NumericUpDown nmAddPrice;
        private System.Windows.Forms.ComboBox cbDeleteSP;
        private System.Windows.Forms.ComboBox cbNewNCC;
        private System.Windows.Forms.NumericUpDown nmNewGia;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbNewMASP;
    }
}