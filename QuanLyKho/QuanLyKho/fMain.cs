﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyKho.DTO;
namespace QuanLyKho
{
    public partial class fMain : Form
    {
        private  DTOTK loginTK;

        public  DTOTK LoginTK
        {
            get { return loginTK; }
            set { loginTK = value; Change(loginTK.Quyen);  }
        }
        public fMain(DTOTK TK)
        {
            InitializeComponent();
            this.LoginTK = TK;
           
        }

        private void fMain_Load(object sender, EventArgs e)
        {

        }

        // form Report
        private void btnReport_Click(object sender, EventArgs e)
        {
            fReport formRP = new fReport();
            this.Hide();
            formRP.ShowDialog();

        }
        //form Nhap Kho
        private void btnImport_Click(object sender, EventArgs e)
        {

            fNhapKho formNK = new fNhapKho();
            this.Hide();
            formNK.ShowDialog();

        }
        // form Xuat Kho
        private void btnExport_Click(object sender, EventArgs e)
        {
            fXuatKho formXK = new fXuatKho();
            this.Hide();
            formXK.ShowDialog();
        }
     
        // Phân Quyền
        private void btnPermisson_Click(object sender, EventArgs e)
        {

            fAdmin formAD = new fAdmin();
            this.Hide();
            formAD.ShowDialog();
        }


        // Thoat
        private void btnExit_Click(object sender, EventArgs e)
        {

            DialogResult result = MessageBox.Show("Bạn có muốn thoát", "Thông báo", MessageBoxButtons.OKCancel);


            switch (result)
            {
                case DialogResult.Cancel:

                    break;
                case DialogResult.OK:
                    this.Close();
                    MessageBox.Show("Đã đóng");
                    break;
                default:
                    break;
            }
        }



        void  Change(string role)
        {
          btnPermisson.Enabled = role == "True";
          btnExport.Enabled = role == "True";
          btnReport.Enabled = role == "True";
        }
       
    }
}
