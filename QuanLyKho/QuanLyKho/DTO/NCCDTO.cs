﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QuanLyKho.DTO
{
    public class NCCDTO
    {
        private int maNCC;
        public int MaNCC { get => maNCC; set => maNCC = value; }
        private string tenNCC;
        public string TenNCC { get => tenNCC; set => tenNCC = value; }

        public NCCDTO(int maNcc, string tenNcc)
        {
            this.MaNCC = maNcc;
            this.TenNCC = tenNcc;
        }

        public NCCDTO(DataRow row)
        {
            this.MaNCC = (int)row["maNCC"];
            this.TenNCC = row["tenNCC"].ToString();
        }
    }
}
