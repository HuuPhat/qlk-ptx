﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace QuanLyKho.DTO
{
    class SPDTO
    {
        private int maSP;
        public int MaSP { get => maSP; set => maSP = value; }
        private float gia;
        public float Gia { get => gia; set => gia = value; }
        private int maNCC;
        private string tenSP;
        public int MaNCC { get => maNCC; set => maNCC = value; }
        public string TenSP { get => tenSP; set => tenSP = value; }

        public SPDTO(int maSP, string tenSP, float gia, int maNCC)
        {
            this.MaSP = maSP;
            this.Gia = gia;
            this.MaNCC = maNCC;
            this.TenSP = tenSP;
        }

        public SPDTO(DataRow row)
        {
            this.MaSP = (int)row["maSP"];
            this.TenSP = (string)row["tenSP"];
            this.Gia = (float)Convert.ToDouble(row["gia"].ToString());
            this.MaNCC = (int)row["maNCC"];
        }
    }
}
