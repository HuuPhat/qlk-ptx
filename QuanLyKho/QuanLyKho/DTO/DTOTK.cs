﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace QuanLyKho.DTO
{
    public class DTOTK
    {
        private int maNV;
        public int MaNV { get => maNV; set => maNV = value; }
        private string tenTK;
        public string TenTK { get => tenTK; set => tenTK = value; }
        private string matKhau;
        public string MatKhau { get => matKhau; set => matKhau = value; }
        private string tenHienThi;
        public string TenHienThi { get => tenHienThi; set => tenHienThi = value; }
        private string quyen;
        public string Quyen { get => quyen; set => quyen = value; }
    

        public DTOTK (int MaNV,string TenTK , string MatKhau ,string TenHienThi, string Quyen)
        {
            this.MaNV = MaNV;
            this.TenTK = TenTK;
            this.MatKhau = MatKhau;
            this.TenHienThi = TenHienThi;
            this.Quyen = Quyen;
        }



        public DTOTK(DataRow row)
        {
            this.MaNV = (int)row["MaNV"];
            this.TenTK = row["TK"].ToString();
            this.MatKhau = row["MK"].ToString();
            this.TenHienThi = row["TenHienThi"].ToString();
            this.Quyen = row["Quyen"].ToString();
        }


    }



}
