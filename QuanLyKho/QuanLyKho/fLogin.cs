﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using QuanLyKho.DAO;
using QuanLyKho.DTO;

namespace QuanLyKho
{
    public partial class fLogin : Form
    {
        public fLogin()
        {
            InitializeComponent();
        }
        private void label1_Click(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void fLogin_Load(object sender, EventArgs e)
        {

        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("thông báo", "Bạn có muốn thoát", MessageBoxButtons.OKCancel);


            switch (result)
            {
                case DialogResult.Cancel:

                    break;  
                case DialogResult.OK:
                    Application.Exit();
                    MessageBox.Show("Đã đóng");
                    break;
                default:
                    break;
            }
        }


        private void btnLogin_Click_1(object sender, EventArgs e)
        {
            
            string userName = tbUsername.Text;
            string passWord = tbPass.Text;
            if (Login(userName, passWord))
            {
               
                DTOTK acc = TKDAO.Instance.GetAccountByUserName(userName);
                fMain f = new fMain(acc);
                this.Hide();
                f.ShowDialog();
                this.Show();
                
                tbUsername.Clear();
                tbPass.Clear();
            }
            else
            {
                MessageBox.Show("Sai tên tài khoản hoặc mật khẩu!");
            }
         
        }
        bool Login(string userName, string passWord)
        {
            return TKDAO.Instance.Login(userName, passWord);
        }

        private void tbPass_TextChanged(object sender, EventArgs e)
        {


        }


    }

    
}

