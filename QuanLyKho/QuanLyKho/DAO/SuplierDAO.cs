﻿using QuanLyKho.DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyKho.DAO
{
    public class SuplierDAO
    {
        private static SuplierDAO instance;

        public static SuplierDAO Instance
        {
            get { if (instance == null) instance = new SuplierDAO(); return instance; }
            private set { instance = value; }
        }

        public List<NCCDTO> GetList()
        {
            List<NCCDTO> list = new List<NCCDTO>();
            string query = "select * from NCC";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach(DataRow item in data.Rows)
            {
                NCCDTO suplier = new NCCDTO(item);
                list.Add(suplier);
            }
            return list;
        }

    }
}
