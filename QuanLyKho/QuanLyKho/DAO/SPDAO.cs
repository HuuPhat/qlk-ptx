﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuanLyKho.DTO;
using System.Data;

namespace QuanLyKho.DAO
{

     class SPDAO
    {
        private static SPDAO instance;

        public static SPDAO Intance
        {
            get { if (instance == null) instance = new SPDAO(); return SPDAO.instance; }
            private set { SPDAO.instance = value; }
        }

        public List<SPDTO> GetSP()
        {
            List<SPDTO> list = new List<SPDTO>();

            string query = "select * from SP";

            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach (DataRow item in data.Rows)
            {
                SPDTO sp = new SPDTO(item);
                list.Add(sp);
            }
            return list;
        }
        public bool InsertSP(string name, float price,int MaNCC)
        {
            string query = string.Format("INSERT SP ( TenSP, Gia, MaNCC )VALUES  ( N'{0}', {1}, {2})", name, price, MaNCC);
            int result = DataProvider.Instance.ExecuteNonQuery(query);

            return result > 0;
        }

        public bool UpdateSP(string TenSP,float price,int MaNCC, int MaSp)
        {
            string query = string.Format(" Update SP set TenSP = N'{0}', Gia = {1}, MaNCC = {2} where MaSP = {3}", TenSP, price, MaNCC, MaSp);    
            int result = DataProvider.Instance.ExecuteNonQuery(query);

            return result > 0;
        }

        public bool DeleteSP(int MaSp)
        {
            string query = string.Format("Delete SP where MaSP = {0}", MaSp);
            int result = DataProvider.Instance.ExecuteNonQuery(query);

            return result > 0;
        }
    }
}
