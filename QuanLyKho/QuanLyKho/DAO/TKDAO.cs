﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using QuanLyKho.DTO;

namespace QuanLyKho.DAO
{
    class TKDAO
    {
        private static TKDAO instance;

        public static TKDAO Instance
        {
            get { if (instance == null) instance = new TKDAO(); return instance; }
            private set { instance = value; }
        }


        private TKDAO() { }

        public bool Login(string userName , string passWord )
        {
            string query = "SELECT * FROM NV WHERE TK = N'" + userName + "' AND MK = N'" + passWord + "' ";

            DataTable result = DataProvider.Instance.ExecuteQuery(query);

            return result.Rows.Count > 0;
        }

        public DTOTK GetAccountByUserName(string username)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("select * from NV where TK ='" + username + "'");
            foreach (DataRow item in data.Rows)
            {
                return new DTOTK(item);
            }
            return null;

        }
        // add tk
        public bool Insert(string name, string displayname, string pass, bool quyen)
        {
            string query = string.Format("insert NV (TK, MK, TenHienThi ,Quyen) values (N'{0}',N'{1}',N'{2}',N'{3}')", name, displayname, pass, quyen);
            int results = DataProvider.Instance.ExecuteNonQuery(query);
            return results > 0;

        }
        //xóa TK
        public bool DeleteAccount(string name)
        {
            string query = string.Format("Delete NV where TK = N'{0}'", name);
            int result = DataProvider.Instance.ExecuteNonQuery(query);

            return result > 0;
        }
        // sửa TK
        public bool UpdateAccount(string name, string displayName, bool type)
        {
            string query = string.Format("UPDATE NV set TenHienThi = N'{1}', Quyen = N'{2}' WHERE TK = N'{0}'", name, displayName, type);
            int result = DataProvider.Instance.ExecuteNonQuery(query);

            return result > 0;
        }

        public List<DTOTK> GetUsername()
        {
            List<DTOTK> list = new List<DTOTK>();
            string query = "select * from NV";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);

            foreach(DataRow item in data.Rows)
            {
                DTOTK account = new DTOTK(item);
                list.Add(account);
            }
            return list;
        }
    }
}
