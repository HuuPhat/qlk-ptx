﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QuanLyKho.DTO;
using System.Data.SqlClient;
using QuanLyKho.DAO;

namespace QuanLyKho
{
    public partial class fAdmin : Form
    {
        private DTOTK loginAccount;

        public DTOTK LoginAccount
        {
            get { return loginAccount; }
            set { loginAccount = value; }
        }

        
        public fAdmin()
        {
            InitializeComponent();
            LoadData();
        }

        #region Methods
        void LoadData()
        {
            LoadUsername(cbDeleteAccount);
            LoadSuplierName(cbIdSuplier);
            LoadProduct(cbDeleteSP);
            LoadSuplierName(cbNewNCC);
            LoadProduct(cbNewMASP);
        }

        void LoadUsername(ComboBox cb)
        {
            cb.DataSource = TKDAO.Instance.GetUsername();
            cb.DisplayMember = "TenTK";
        }

        void LoadSuplierName(ComboBox cb)
        {
            cb.DataSource = SuplierDAO.Instance.GetList();
            cb.DisplayMember = "TenNCC";
        }
        #region Account
        void AddAccount(string name, string displayname, string pass, bool role)
        {
            if(TKDAO.Instance.Insert(name,displayname,pass,role))
            {
                MessageBox.Show("Thêm tài khoản thành công !","Thông Báo",MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Thêm tài khoản không thành công !","Thông Báo",MessageBoxButtons.OK);
            }
        }

        void EditAccount(string name, string displayName, bool roles)
        {
            if (TKDAO.Instance.UpdateAccount(name, displayName, roles))
            {
                MessageBox.Show("Cập nhật tài khoản thành công !","Thông Báo",MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Cập nhật tài khoản không thành công !","Thông Báo",MessageBoxButtons.OK);
            }
        }   

        void DeleteAccount(string name)
        {
            if(TKDAO.Instance.DeleteAccount(name))
            {
                MessageBox.Show("Xóa tài khoản thành công !","Thông Báo",MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Xóa tài khoản không thành công !","Thông Báo",MessageBoxButtons.OK);
            }
        }
        #endregion


        #region Product
        void LoadProduct(ComboBox cb)
        {
            cb.DataSource = SPDAO.Intance.GetSP();
            cb.DisplayMember = "TenSP";
        }
        #endregion
        #endregion

        #region Events 
        private void btnDangKy_Click_1(object sender, EventArgs e)
        {
            string name = TK.Text;
            string pass = MK.Text;
            string displayname = HoVaTen.Text;
            bool role = checkAD.CheckState.Equals(CheckState.Checked);
            if (name != "" && displayname != "" && pass != "")
            {
                AddAccount(name, displayname, pass, role);
            }
            else
            {
                MessageBox.Show("Bạn Chưa Điền Đầy Đủ Thông Tin !", "Thông Báo", MessageBoxButtons.OK);
            }
        }

        private void btnXOA_Click(object sender, EventArgs e)
        {
            string name = (cbDeleteAccount.SelectedItem as DTOTK).TenTK;
            if (loginAccount.TenTK.Equals(name))
            {
                MessageBox.Show("Vui lòng đừng xóa chính bạn chứ");
                return;
            }
            DeleteAccount(name);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string displayName = tbEditDisplayName.Text;
            string name = tbEditUsername.Text;
            bool type = chbEditRole.Checked;
            if (TKDAO.Instance.UpdateAccount(name, displayName, type))
            {
                MessageBox.Show("Cập nhật tài khoản thành công");
            }
            else
            {
                MessageBox.Show("Cập nhật tài khoản thất bại");
            }

        }
       
        #endregion

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = tbTenSp.Text;
            float price = (float)nmAddPrice.Value;
            int idSuplier = (cbIdSuplier.SelectedItem as NCCDTO).MaNCC; // ???
            if (SPDAO.Intance.InsertSP(name, price, idSuplier))
            {
                MessageBox.Show("Thêm Sản Phẩm thành công");

            }
            else
            {
                MessageBox.Show("Có lỗi khi thêm Sản Phẩm");
            }
        }

        private void btnXoaSP_Click(object sender, EventArgs e)
        {
            int idSP = (cbDeleteSP.SelectedItem as SPDTO).MaSP;
            if (SPDAO.Intance.DeleteSP(idSP))
            {
                MessageBox.Show("Xóa Sản Phẩm thành công");

            }
            else
            {
                MessageBox.Show("Có lỗi khi xóa Sản Phẩm");
            }
        }

        private void cbIdSuplier_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void EditSP_Click(object sender, EventArgs e)
        {
            int idSP = (cbNewMASP.SelectedItem as SPDTO).MaSP;
            string NewName = tbNewname.Text;
            int newMaNCC = (cbNewNCC.SelectedItem as NCCDTO).MaNCC;
            float newGia = (float)nmNewGia.Value;

            if (SPDAO.Intance.UpdateSP(NewName,newGia,newMaNCC,idSP))
            {
                MessageBox.Show("sửa Sản Phẩm thành công");

            }
            else
            {
                MessageBox.Show("Có lỗi khi sửa Sản Phẩm");
            }


        }
    }
}

